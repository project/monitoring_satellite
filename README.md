# Monitoring Satellite for Drupal 🛰

The Monitoring Satellite provides data about your Drupal CMS for the Monitoring Station.

### Installation
```bash
$ composer require drupal/monitoring_satellite
```

## Configuration
Go to `/admin/config/system/monitoring-satellite` and add username and password for the basic authentication.

## Test
Call `/monitoring-satellite/v1/get` with the authentication headers.

Example
```bash
$ curl --user foo:bar --request GET 'https://localhost/monitoring-satellite/v1/get'
```

It should be protected by basic authentication and return the data after successful authentication.

## Add the Satellite to the Station
Add this Monitoring Satellite to the Monitoring Station. See [documentation of the Monitoring Station](https://github.com/marcosimbuerger/monitoring-station).

## License
This bundle is released under the GPL license. See the included [LICENSE](LICENSE.txt) file for more information.
