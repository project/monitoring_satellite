<?php

declare(strict_types=1);

namespace Drupal\monitoring_satellite\Controller;

use Drupal as Drupal;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class MonitoringSatelliteController.
 *
 * @package Drupal\monitoring_satellite\Controller
 */
class MonitoringSatelliteController {

  /**
   * Get the Drupal CMS data.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The Drupal CMS data as JSON response.
   */
  public function get(): JsonResponse {
    return new JsonResponse([
      'app' => 'Drupal',
      'versions' => [
        'app' => Drupal::VERSION,
        'php' => phpversion(),
      ],
    ]);
  }

}
