<?php

declare(strict_types=1);

namespace Drupal\monitoring_satellite\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\FormBase;

/**
 * Class MonitoringSatelliteSettingsForm.
 *
 * @package Drupal\monitoring_satellite\Form
 */
class MonitoringSatelliteSettingsForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'monitoring_satellite_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['basic_auth'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('HTTP basic authentication credentials'),
    ];

    $form['basic_auth']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $this->config('monitoring_satellite.settings')->get('basic_auth.username'),
      '#required' => TRUE,
    ];

    $form['basic_auth']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $this->config('monitoring_satellite.settings')->get('basic_auth.password'),
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $editableConfig = $this->configFactory()->getEditable('monitoring_satellite.settings');
    $editableConfig->set('basic_auth.username', $form_state->getValue('username'))->save();
    $editableConfig->set('basic_auth.password', $form_state->getValue('password'))->save();
  }

}
