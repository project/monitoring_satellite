<?php

declare(strict_types=1);

namespace Drupal\monitoring_satellite\Access;

use Drupal\Component\Utility\Html;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class MonitoringSatelliteBasicAuthAccessCheck.
 *
 * @package Drupal\monitoring_satellite\Access
 */
class MonitoringSatelliteBasicAuthAccessCheck implements AccessInterface {

  /**
   * The Monitoring Satellite route name for 'get'.
   *
   * @var string
   */
  private const MONITORING_SATELLITE_ROUTE_NAME_GET = 'monitoring_satellite.get';

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  private ?Request $request;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  private CurrentRouteMatch $currentRouteMatch;

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\Config
   */
  private Config $config;

  /**
   * MonitoringSatelliteBasicAuthAccessCheck constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   The request stack.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $currentRouteMatch
   *   The current route match.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The configuration factory.
   */
  public function __construct(RequestStack $requestStack, CurrentRouteMatch $currentRouteMatch, ConfigFactory $configFactory) {
    $this->request = $requestStack->getCurrentRequest();
    $this->currentRouteMatch = $currentRouteMatch;
    $this->config = $configFactory->get('monitoring_satellite.settings');
  }

  /**
   * Check access.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(): AccessResultInterface {
    $routeName = $this->currentRouteMatch->getRouteName();

    if ($routeName && $routeName === self::MONITORING_SATELLITE_ROUTE_NAME_GET) {
      if ($this->checkIfConfigIsValid() === FALSE || $this->checkIfBasicAuthCredentialsAreValid($this->request) === FALSE) {
        return AccessResult::forbidden();
      }
    }

    // Default. Allow if it is not the Monitoring Satellite route.
    return AccessResult::allowed();
  }

  /**
   * Check if the configuration is valid.
   *
   * @return bool
   *   TRUE if valid,
   *   FALSE otherwise.
   */
  private function checkIfConfigIsValid(): bool {
    if (!empty($this->config->get('basic_auth.username')) && !empty($this->config->get('basic_auth.password'))) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Checks if the HTTP basic authentication credentials are valid for the given request.
   *
   * TODO: Create an own authentication provider when Drupal allows it to do so.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   *
   * @return bool
   *   TRUE if the HTTP basic authentication credentials are valid,
   *   FALSE otherwise.
   */
  private function checkIfBasicAuthCredentialsAreValid(Request $request): bool {
    // PHP_AUTH_USER and PHP_AUTH_PW is also set if HTTP_AUTHORIZATION or REDIRECT_HTTP_AUTHORIZATION is sent.
    // See: Symfony\Component\HttpFoundation\ServerBag::getHeaders.

    if ($request->server->has('PHP_AUTH_USER') && $request->server->has('PHP_AUTH_PW')) {
      $username = Html::escape($request->server->get('PHP_AUTH_USER'));
      $password = Html::escape($request->server->get('PHP_AUTH_PW'));

      if ($username === $this->config->get('basic_auth.username') &&
        $password === $this->config->get('basic_auth.password')
      ) {
        return TRUE;
      }
    }

    return FALSE;
  }

}
