<?php

declare(strict_types=1);

namespace Drupal\Tests\monitoring_satellite\Unit\Access;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\monitoring_satellite\Access\MonitoringSatelliteBasicAuthAccessCheck;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * MonitoringSatelliteBasicAuthAccessCheckTest.
 *
 * @coversDefaultClass \Drupal\monitoring_satellite\Access\MonitoringSatelliteBasicAuthAccessCheck
 * @group Access
 * @group monitoring_satellite
 */
class MonitoringSatelliteBasicAuthAccessCheckTest extends UnitTestCase {

  /**
   * The Monitoring Satellite route name for 'get'.
   *
   * @var string
   */
  private const MONITORING_SATELLITE_ROUTE_NAME_GET = 'monitoring_satellite.get';

  /**
   * Get the RequestStack mock.
   *
   * @param array $serverParameters
   *   The SERVER parameters.
   *
   * @return \Symfony\Component\HttpFoundation\RequestStack
   *   The RequestStack mock.
   */
  protected function getRequestStackMock(array $serverParameters = []): RequestStack {
    $request = new Request([], [], [], [], [], $serverParameters);

    $requestStackMock = $this->getMockBuilder(RequestStack::class)
      ->disableOriginalConstructor()
      ->getMock();
    $requestStackMock->method('getCurrentRequest')->willReturn($request);

    return $requestStackMock;
  }

  /**
   * Get the CurrentRouteMatch mock.
   *
   * @param string|null $routeName
   *   The route name.
   *
   * @return \Drupal\Core\Routing\CurrentRouteMatch
   *   The CurrentRouteMatch mock.
   */
  protected function getCurrentRouteMatchMock(?string $routeName): CurrentRouteMatch {
    $currentRouteMatchMock = $this->getMockBuilder(CurrentRouteMatch::class)
      ->disableOriginalConstructor()
      ->getMock();
    $currentRouteMatchMock->method('getRouteName')->willReturn($routeName);

    return $currentRouteMatchMock;
  }

  /**
   * Get the ConfigFactory mock.
   *
   * @param string|null $username
   *   The username in the config.
   * @param string|null $password
   *   The password in the config.
   *
   * @return \Drupal\Core\Config\ConfigFactory
   *   The ConfigFactory mock.
   */
  protected function getConfigFactoryMock(?string $username, ?string $password): ConfigFactory {
    $configMock = $this->getMockBuilder(Config::class)
      ->disableOriginalConstructor()
      ->getMock();

    $configMock->expects($this->any())
      ->method('get')->will($this->returnValueMap([
        ['basic_auth.username', $username],
        ['basic_auth.password', $password],
      ]));

    $configFactoryMock = $this->getMockBuilder(ConfigFactory::class)
      ->disableOriginalConstructor()
      ->getMock();
    $configFactoryMock->method('get')->with('monitoring_satellite.settings')->willReturn($configMock);

    return $configFactoryMock;
  }

  /**
   * Get the MonitoringSatelliteBasicAuthAccessCheck object.
   *
   * @param array $serverParameters
   *   The SERVER parameters.
   * @param string|null $routeName
   *   The route name.
   * @param string|null $username
   *   The username in the config.
   * @param string|null $password
   *   The password in the config.
   *
   * @return \Drupal\monitoring_satellite\Access\MonitoringSatelliteBasicAuthAccessCheck
   *   The MonitoringSatelliteBasicAuthAccessCheck object.
   */
  protected function getMonitoringSatelliteBasicAuthAccessCheck(array $serverParameters, ?string $routeName, ?string $username, ?string $password): MonitoringSatelliteBasicAuthAccessCheck {
    return new MonitoringSatelliteBasicAuthAccessCheck(
      $this->getRequestStackMock($serverParameters),
      $this->getCurrentRouteMatchMock($routeName),
      $this->getConfigFactoryMock($username, $password)
    );
  }

  /**
   * Data provider for testAccess().
   *
   * @return iterable
   *   The data.
   */
  public function accessCheckDataProvider(): iterable {
    // Missing route name.
    yield [
      'accessCheckObject' => $this->getMonitoringSatelliteBasicAuthAccessCheck(
        [],
        NULL,
        NULL,
        NULL,
      ),
      'expectedResult' => AccessResultAllowed::class,
    ];

    // Wrong route name.
    yield [
      'accessCheckObject' => $this->getMonitoringSatelliteBasicAuthAccessCheck(
        [],
        'route-xy',
        NULL,
        NULL,
      ),
      'expectedResult' => AccessResultAllowed::class,
    ];

    // Missing config values.
    yield [
      'accessCheckObject' => $this->getMonitoringSatelliteBasicAuthAccessCheck(
        [],
        self::MONITORING_SATELLITE_ROUTE_NAME_GET,
        NULL,
        NULL,
      ),
      'expectedResult' => AccessResultForbidden::class,
    ];

    // Missing username config values.
    yield [
      'accessCheckObject' => $this->getMonitoringSatelliteBasicAuthAccessCheck(
        [],
        self::MONITORING_SATELLITE_ROUTE_NAME_GET,
        NULL,
        'bar',
      ),
      'expectedResult' => AccessResultForbidden::class,
    ];

    // Missing password config values.
    yield [
      'accessCheckObject' => $this->getMonitoringSatelliteBasicAuthAccessCheck(
        [],
        self::MONITORING_SATELLITE_ROUTE_NAME_GET,
        'foo',
        NULL,
      ),
      'expectedResult' => AccessResultForbidden::class,
    ];

    // Missing server parameters.
    yield [
      'accessCheckObject' => $this->getMonitoringSatelliteBasicAuthAccessCheck(
        [],
        self::MONITORING_SATELLITE_ROUTE_NAME_GET,
        'foo',
        'bar',
      ),
      'expectedResult' => AccessResultForbidden::class,
    ];

    // Missing PHP_AUTH_USER server parameter.
    yield [
      'accessCheckObject' => $this->getMonitoringSatelliteBasicAuthAccessCheck(
        [
          'PHP_AUTH_PW' => 'bar'
        ],
        self::MONITORING_SATELLITE_ROUTE_NAME_GET,
        'foo',
        'bar',
      ),
      'expectedResult' => AccessResultForbidden::class,
    ];

    // Missing PHP_AUTH_PW server parameter.
    yield [
      'accessCheckObject' => $this->getMonitoringSatelliteBasicAuthAccessCheck(
        [
          'PHP_AUTH_USER' => 'foo',
        ],
        self::MONITORING_SATELLITE_ROUTE_NAME_GET,
        'foo',
        'bar',
      ),
      'expectedResult' => AccessResultForbidden::class,
    ];

    // Username not identical.
    yield [
      'accessCheckObject' => $this->getMonitoringSatelliteBasicAuthAccessCheck(
        [
          'PHP_AUTH_USER' => 'foo',
          'PHP_AUTH_PW' => 'bar',
        ],
        self::MONITORING_SATELLITE_ROUTE_NAME_GET,
        'xxx',
        'bar',
      ),
      'expectedResult' => AccessResultForbidden::class,
    ];

    // Password not identical.
    yield [
      'accessCheckObject' => $this->getMonitoringSatelliteBasicAuthAccessCheck(
        [
          'PHP_AUTH_USER' => 'foo',
          'PHP_AUTH_PW' => 'bar',
        ],
        self::MONITORING_SATELLITE_ROUTE_NAME_GET,
        'foo',
        'xxx',
      ),
      'expectedResult' => AccessResultForbidden::class,
    ];

    // Working example.
    yield [
      'accessCheckObject' => $this->getMonitoringSatelliteBasicAuthAccessCheck(
        [
          'PHP_AUTH_USER' => 'foo',
          'PHP_AUTH_PW' => 'bar',
        ],
        self::MONITORING_SATELLITE_ROUTE_NAME_GET,
        'foo',
        'bar',
      ),
      'expectedResult' => AccessResultAllowed::class,
    ];
  }

  /**
   * Tests the constructor.
   *
   * @covers ::__construct
   */
  public function testMonitoringSatelliteBasicAuthAccessCheck(): void {
    $monitoringSatelliteBasicAuthAccessCheck = $this->getMonitoringSatelliteBasicAuthAccessCheck(
      [],
      NULL,
      NULL,
      NULL,
    );
    $this->assertInstanceOf(MonitoringSatelliteBasicAuthAccessCheck::class, $monitoringSatelliteBasicAuthAccessCheck);
  }

  /**
   * Tests the access method.
   *
   * @covers ::access
   * @dataProvider accessCheckDataProvider
   */
  public function testAccess(MonitoringSatelliteBasicAuthAccessCheck $accessCheckObject, string $expectedResult): void {
    $this->assertInstanceOf($expectedResult, $accessCheckObject->access());
  }

}
